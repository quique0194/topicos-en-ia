from random import random, shuffle, randint
import numpy as np

INF = 999999

def euclid_distance(a, b):
    return (((b-a)**2).sum())**0.5

def kmeans(k, data):
    # init centers
    centers = np.zeros((k, data.shape[1]))
    for i, center in enumerate(centers):
        centers[i] = data[i]

    # init categories
    categories = np.zeros(data.shape[0])

    for _ in xrange(100):
        # set categories
        for i, obj in enumerate(data):
            min = INF
            for j, center in enumerate(centers):
                dist = euclid_distance(obj, center)
                if dist < min:
                    min = dist
                    categories[i] = j

        # recalculate centers
        for j, center in enumerate(centers):
            suma = np.zeros(data.shape[1])
            cont = 0
            for i, obj in enumerate(data):
                if categories[i] == j:
                    suma += obj
                    cont += 1
            centers[j] = suma/cont

    return centers



def main():    
    data = np.random.sample((1000, 2))
    centers = kmeans(4, data)
    for center in centers:
        print center
    

main()