from mlp import MLP
from numpy import array, arange, pi, sin, zeros
from matplotlib.pyplot import plot, show, draw, pause, axes
from random import shuffle


if __name__ == '__main__':
    X = arange(-pi, pi, 0.1)
    Y = sin(X)
    OBT = zeros(len(X))

    plot(X, Y, '-b')
    obt_plot, = plot(X, OBT, '*r')
    draw()

    mlp = MLP([1, 10, 1])
    for epoc in range(10000):
        pos = range(len(X))
        shuffle(pos)
        for i in pos:
            input = [X[i]]
            desired = [Y[i]]
            OBT[i] = mlp.train(input, desired)[0]
        obt_plot.set_data(X, OBT)
        draw()
        pause(0.0001)
