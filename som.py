from random import random, shuffle, randint
import numpy as np
from Queue import PriorityQueue
import matplotlib.pyplot as plt


INF = 999999
alpha = 0.1

def euclid_distance(a, b):
    return (((b-a)**2).sum())**0.5

def kmeans(k, data):
    # init centers
    centers = np.zeros((k, data.shape[1]))
    for i, center in enumerate(centers):
        centers[i] = data[i]

    # init categories
    categories = np.zeros(data.shape[0])

    for _ in xrange(100):
        # set categories
        for i, obj in enumerate(data):
            min = INF
            for j, center in enumerate(centers):
                dist = euclid_distance(obj, center)
                if dist < min:
                    min = dist
                    categories[i] = j

        # recalculate centers
        for j, center in enumerate(centers):
            suma = np.zeros(data.shape[1])
            cont = 0
            for i, obj in enumerate(data):
                if categories[i] == j:
                    suma += obj
                    cont += 1
            centers[j] = suma/cont

    return centers


def som1(k, data, epochs=500):
    # init centers
    centers = np.zeros((k, data.shape[1]))
    for i, center in enumerate(centers):
        centers[i] = data[i]

    for _ in xrange(epochs):
        print _
        for i, obj in enumerate(data):
            for j, center in enumerate(centers):
                centers[j] = (1-alpha)*center + alpha*obj

    return centers


def som2(k, data, epochs=500):
    # init centers
    centers = np.zeros((k, data.shape[1]))
    for i, center in enumerate(centers):
        centers[i] = data[i]

    for _ in xrange(epochs):
        print _
        for i, obj in enumerate(data):
            nearest_idx = None
            min_dist = INF
            for j, center in enumerate(centers):
                dist = euclid_distance(center, obj)
                if dist < min_dist:
                    min_dist = dist
                    nearest_idx = j
            centers[nearest_idx] = (1-alpha)*centers[nearest_idx] + alpha*obj

    return centers


def som3(k, data, epochs=500):
    # init centers
    centers = np.zeros((k, data.shape[1]))
    for i, center in enumerate(centers):
        centers[i] = data[i]

    # init vector K
    K = np.arange(1, 0, -1.0/k)

    for _ in xrange(epochs):
        print _
        for i, obj in enumerate(data):
            pq = PriorityQueue()
            for j, center in enumerate(centers):
                dist = euclid_distance(center, obj)
                pq.put((dist, j))
            for force in K:
                center_idx = pq.get()[1]
                centers[center_idx] += alpha*(obj-centers[center_idx])*force

    return centers


def som3_plot(k, data, epochs=500):
    # init centers
    centers = np.zeros((k, data.shape[1]))
    for i, center in enumerate(centers):
        centers[i] = data[i]

    # init vector K
    K = np.arange(1, 0, -1.0/k)

    pnts, = plt.plot(centers.transpose()[0], centers.transpose()[1], 'or')

    for _ in xrange(epochs):
        print _
        for i, obj in enumerate(data):
            pq = PriorityQueue()
            for j, center in enumerate(centers):
                dist = euclid_distance(center, obj)
                pq.put((dist, j))
            for force in K:
                center_idx = pq.get()[1]
                centers[center_idx] += alpha*(obj-centers[center_idx])*force
        pnts.set_data(centers.transpose()[0], centers.transpose()[1])
        plt.draw()
        plt.pause(0.001)

    return centers



def kohonen_plot(k, data, epochs=500, vecindad=1):
    """
    Topology here is a circular list
    """
    # init centers
    centers = np.zeros((k, data.shape[1]))
    for i, center in enumerate(centers):
        centers[i] = data[i]

    # init vector K
    K = np.arange(1, 0, -1.0/k)

    pnts, = plt.plot(centers.transpose()[0], centers.transpose()[1], '-r')

    for _ in xrange(epochs):
        print _
        for i, obj in enumerate(data):
            nearest_idx = None
            min_dist = INF
            for j, center in enumerate(centers):
                dist = euclid_distance(center, obj)
                if dist < min_dist:
                    min_dist = dist
                    nearest_idx = j
            for v in xrange(vecindad):
                centers[(nearest_idx+v)%k] += alpha*(obj-centers[(nearest_idx+v)%k])*(0.5**v)
                centers[(nearest_idx-v)%k] += alpha*(obj-centers[(nearest_idx-v)%k])*(0.5**v)
        pnts.set_data(centers.transpose()[0], centers.transpose()[1])
        plt.draw()
        plt.pause(0.001)

    return centers


def kohonen2_plot(k, data, epochs=500, vecindad=1):
    """
    Topology here is a non circular list
    """
    # init centers
    centers = np.zeros((k, data.shape[1]))
    for i, center in enumerate(centers):
        centers[i] = data[i]

    # init vector K
    K = np.arange(1, 0, -1.0/k)

    pnts, = plt.plot(centers.transpose()[0], centers.transpose()[1], '-r')

    for _ in xrange(epochs):
        print _
        for i, obj in enumerate(data):
            nearest_idx = None
            min_dist = INF
            for j, center in enumerate(centers):
                dist = euclid_distance(center, obj)
                if dist < min_dist:
                    min_dist = dist
                    nearest_idx = j
            for v in xrange(vecindad):
                if nearest_idx+v < k:
                    centers[nearest_idx+v] += alpha*(obj-centers[nearest_idx+v])*(0.5**v)
                if nearest_idx-v >= 0:
                    centers[nearest_idx-v] += alpha*(obj-centers[nearest_idx-v])*(0.5**v)
        pnts.set_data(centers.transpose()[0], centers.transpose()[1])
        plt.draw()
        plt.pause(0.001)

    return centers


def kohonen3_plot(k, data, epochs=500, vecindad=1):
    """
    Topology here is a matrix (k,k)
    """
    # dimensions
    dim = data.shape[1]
    # init centers
    centers = np.zeros((k, k, dim))
    for i, row in enumerate(centers):
        for j, center in enumerate(row):
            centers[i][j] = data[i]

    # init vector K
    K = np.arange(1, 0, -1.0/k)

    pnts, = plt.plot(centers.reshape((k*k, dim)).transpose()[0],
                     centers.reshape((k*k, dim)).transpose()[1], '-r')

    for _ in xrange(epochs):
        print _
        for i, obj in enumerate(data):
            nearest_idx = None
            min_dist = INF
            for i, row in enumerate(centers):
                for j, center in enumerate(row):
                    dist = euclid_distance(center, obj)
                    if dist < min_dist:
                        min_dist = dist
                        nearest_idx = i, j
            # move the nearest
            centers[nearest_idx] += alpha*(obj-centers[nearest_idx])
            for v in xrange(1, vecindad):
                # right
                idx = nearest_idx[0], nearest_idx[1] + v
                if idx[0] >= 0 and idx[0] < k and idx[1] >= 0 and idx[1] < k:
                    centers[idx] += alpha*(obj-centers[idx])*(0.5**v)
                # top right
                idx = nearest_idx[0] + v, nearest_idx[1] + v
                if idx[0] >= 0 and idx[0] < k and idx[1] >= 0 and idx[1] < k:
                    centers[idx] += alpha*(obj-centers[idx])*(0.5**v)
                # top
                idx = nearest_idx[0] + v, nearest_idx[1]
                if idx[0] >= 0 and idx[0] < k and idx[1] >= 0 and idx[1] < k:
                    centers[idx] += alpha*(obj-centers[idx])*(0.5**v)
                # top left
                idx = nearest_idx[0] + v, nearest_idx[1] - v
                if idx[0] >= 0 and idx[0] < k and idx[1] >= 0 and idx[1] < k:
                    centers[idx] += alpha*(obj-centers[idx])*(0.5**v)
                # left
                idx = nearest_idx[0] , nearest_idx[1] - v
                if idx[0] >= 0 and idx[0] < k and idx[1] >= 0 and idx[1] < k:
                    centers[idx] += alpha*(obj-centers[idx])*(0.5**v)
                # bottom left
                idx = nearest_idx[0] - v, nearest_idx[1] - v
                if idx[0] >= 0 and idx[0] < k and idx[1] >= 0 and idx[1] < k:
                    centers[idx] += alpha*(obj-centers[idx])*(0.5**v)
                # bottom
                idx = nearest_idx[0] - v, nearest_idx[1]
                if idx[0] >= 0 and idx[0] < k and idx[1] >= 0 and idx[1] < k:
                    centers[idx] += alpha*(obj-centers[idx])*(0.5**v)
                # bottom right
                idx = nearest_idx[0] - v, nearest_idx[1] + v
                if idx[0] >= 0 and idx[0] < k and idx[1] >= 0 and idx[1] < k:
                    centers[idx] += alpha*(obj-centers[idx])*(0.5**v)

        pnts.set_data(centers.reshape((k*k, dim)).transpose()[0],
                      centers.reshape((k*k, dim)).transpose()[1])
        plt.draw()
        plt.pause(0.001)

    return centers



def get_square_data(n, dim):
    return np.random.sample((n, dim))

def get_circle_data(n, dim):
    ret = np.zeros((n, dim))
    center = np.array([0.5, 0.5])
    cont = 0
    while cont < n:
        data = np.random.sample((1, dim))
        while euclid_distance(center, data) > 0.5:
            data = np.random.sample((1, dim))
        ret[cont] = data
        cont+=1
    return ret


def main():
    data = get_square_data(1000, 2)
    # data = get_circle_data(1000, 2)

    plt.plot(data.transpose()[0], data.transpose()[1], 'xb')
    centers = kohonen3_plot(6, data, vecindad=2)
    for center in centers:
        print center


main()