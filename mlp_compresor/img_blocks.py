from numpy import arange, zeros, array


def divide_in_blocks(img, side=4):
    blocks = []
    for r in arange(0, img.shape[0], side):
        for c in arange(0, img.shape[1], side):
            block = []
            for i in range(side):
                for j in range(side):
                    block.append(img[r+i, c+j])
            blocks.append(block)
    return array(blocks)


def restore_from_blocks(blocks, shape, side=4):
    """ blocks is np.array """
    img = zeros(shape)
    block_cont = 0
    for r in arange(0, shape[0], side):
        for c in arange(0, shape[1], side):
            for i in range(side):
                for j in range(side):
                    img[r+i, c+j] = blocks[block_cont, side*i + j]
            block_cont += 1
    return img
