from random import shuffle
import pickle

from matplotlib.image import imread
from matplotlib.pyplot import imshow, draw, pause, figure
from numpy import arange, zeros, array, ones

from mlp import MLP
from img_blocks import divide_in_blocks, restore_from_blocks

# side of img_block
SIDE = 4


def main():
    img = imread('img/mountain.jpg')

    # get gray scale, channel R
    img = img[:, :, 0]
    img = img/255.0

    # cut image so as to have sides multiple of SIDE
    rows = img.shape[0]
    cols = img.shape[1]
    img = img[rows % SIDE:, cols % SIDE:]

    # plot image 'before'
    fig = figure()

    a = fig.add_subplot(1, 2, 1)
    a.set_title('before')
    before_plot = imshow(img*255)
    before_plot.set_cmap('gray')
    draw()
    pause(0.1)

    # train network
    blocks = divide_in_blocks(img, SIDE)
    obt_blocks = zeros(blocks.shape)

    a = fig.add_subplot(1, 2, 2)
    a.set_title('after')
    after_plot = imshow(img*255)
    after_plot.set_cmap('gray')

    mlp = MLP([SIDE**2, SIDE**2/4, SIDE**2])
    for epoch in range(10):
        print epoch
        pos = range(len(blocks))
        shuffle(pos)
        for i in pos:
            input = blocks[i]
            desired = blocks[i]
            obt_blocks[i] = mlp.train(input, desired)

        # plot image 'after'
        img_after = restore_from_blocks(obt_blocks, img.shape, SIDE)
        after_plot.set_data(img_after*255)
        draw()
        pause(0.1)

    # save mlp to file
    with open('compresor_data.pkl', 'wb') as output:
        pickle.dump(mlp, output, pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    main()
