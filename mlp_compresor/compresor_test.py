from random import shuffle
import pickle

from matplotlib.image import imread
from matplotlib.pyplot import imshow, draw, pause, figure, show
from numpy import arange, zeros, array, ones

from mlp import MLP
from img_blocks import divide_in_blocks, restore_from_blocks

# side of img_block
SIDE = 4


def main():
    img = imread('img/mountain.jpg')

    # get gray scale, channel R
    img = img[:, :, 0]
    img = img/255.0

    # cut image so as to have sides multiple of SIDE
    rows = img.shape[0]
    cols = img.shape[1]
    img = img[rows % SIDE:, cols % SIDE:]

    # plot image 'original'
    fig = figure()

    a = fig.add_subplot(2, 2, 1)
    a.set_title('Original')
    before_plot = imshow(img*255)
    before_plot.set_cmap('gray')
    draw()

    # load mlp
    with open('compresor_data.pkl', 'rb') as input:
        mlp = pickle.load(input)

    # compress img
    blocks = divide_in_blocks(img, SIDE)
    obt_shape = blocks.shape[0], blocks.shape[1]/4
    obt_blocks = zeros(obt_shape)
    pos = range(len(blocks))
    for i in pos:
        input = blocks[i]
        obt_blocks[i] = mlp.partial_proc(input, 0, 1)

    # plot image 'compressed'
    a = fig.add_subplot(2, 2, 2)
    a.set_title('Compressed')

    after_shape = img.shape[0]/2, img.shape[1]/2
    img_after = restore_from_blocks(obt_blocks, after_shape, SIDE/2)
    after_plot = imshow(img_after*255)
    after_plot.set_cmap('gray')

    # descompress img
    obt_blocks = zeros(blocks.shape)
    pos = range(len(blocks))
    for i in pos:
        input = blocks[i]
        obt_blocks[i] = mlp.proc(input)

    # plot image 'descompressed'
    a = fig.add_subplot(2, 2, 3)
    a.set_title('Descompressed')
    img_after = restore_from_blocks(obt_blocks, img.shape, SIDE)
    after_plot = imshow(img_after*255)
    after_plot.set_cmap('gray')
    show()


if __name__ == '__main__':
    main()
