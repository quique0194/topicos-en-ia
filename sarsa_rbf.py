import numpy as np
from numpy.linalg import norm

#########
# World #
#########

# STATES
# 1 2 3
# 4 5 6
# 7 8 9

S = 9 # number of states
A = 4 # number of actions

def reward(s):
    if s == 9:
        return 1.0
    elif s >= 1 and s <= 9:
        return 0.0
    else:
        return -1

def execute(s, a):
    if a == 1:      # go right
        ret = s+1
    elif a == 2:    # go down
        ret = s+3
    elif a == 3:    # go left
        ret = s-1
    elif a == 4:    # go up
        ret = s-3
    return ret if reward(ret) != -1 else s

def next_action(s):
    a = np.random.randint(1,5)
    return a

#############
# Sarsa RBF #
#############

alfa = 0.2
lmda = 0.3
gama = 0.95
nw = 0.05
nc = 0.05
no2 = 0.05

N = 3
w = np.random.random(N)
c = 10*np.random.random((N, 2))
o2 = np.random.random(N)

def phi(s,a,k):
    u = np.array([s, a], dtype='double')
    return np.exp(-(norm(u-c[k])**2/o2[k]))

def proc(s, a):
    res = 0.0
    for k in xrange(N):
        res += w[k]*phi(s,a,k)
    return res

for _ in xrange(100):
    st = 1
    at = 1
    et = np.zeros((S+1, A+1), dtype='double')
    visited = np.array([[st, at]], dtype='double')

    while reward(st) < 1:
        st_1 = execute(st, at)
        at_1 = next_action(st_1)
        rt_1 = reward(st_1)

        yt = proc(st, at)
        yt_1 = proc(st_1, at_1)

        dt = rt_1 + lmda*yt_1 - yt
        et[st,at] += 1

        ut = np.array([st, at], dtype='double')
        ut_1 = np.array([st_1, at_1], dtype='double')
        for v in visited:
            s = v[0]
            a = v[1]
            e = alfa*dt*et[s, a]
            for k in xrange(N):
                w[k] -= nw*e*alfa*(gama*phi(st_1, at_1, k) - phi(st, at, k))*et[s,a]
                c[k] -= 2*nc*e*alfa*w[k]*et[s,a]*(gama*(ut_1-c[k])*phi(st_1, at_1, k) - (ut-c[k])*phi(st,at,k))/o2[k]
                o2[k] -= no2*e*alfa*w[k]*et[s,a]*(gama*norm(ut_1-c[k])**2*phi(st_1, at_1, k) - norm(ut-c[k])**2*phi(st,at,k))/o2[k]**2
            et[s,a] = gama*lmda*et[s,a]
        
        st = st_1
        at = at_1
        np.append(visited, np.array([[st, at]]), axis=0)


###########
# Results #
###########

def print_board():
    for s in xrange(1, 10):
        for a in xrange(1, 5):
            print s, a, proc(s, a)

print_board()
