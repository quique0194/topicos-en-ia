import matplotlib.pyplot as plt
import numpy as np
import time
import random


A = ([1, 1.1, 1.2, 1.2, 1.8, 1.1, 1.2, 1.15, 1.9],
     [1, 1.1, 1.2, 1.3, 1.9, 1.8, 2.2, 1.05, 1.25])

B = ([1.3, 1.3, 1.4, 1.4, 1.5, 1.4, 1.7],
     [2.1, 2.2, 2.3, 2.4, 2.5, 2.4, 2.25])

# this is A union B
DATA = ([1, 1.1, 1.2, 1.2, 1.8, 1.1, 1.2, 1.15,  1.9, 1.3, 1.3, 1.4, 1.4, 1.5, 1.4,  1.7],
        [1, 1.1, 1.2, 1.3, 1.9, 1.8, 2.2, 1.05, 1.25, 2.1, 2.2, 2.3, 2.4, 2.5, 2.4, 2.25],
        [1,   1,   1,   1,   1,   1,   1,    1,    1,  -1,  -1,  -1,  -1,  -1,  -1,   -1])


def main():
    """
    Function main
    """
    # plot points of groups A and B
    plt.plot(A[0], A[1], 'bx')
    plt.plot(B[0], B[1], 'ro')

    # create line
    xdata = np.arange(1, 2, 0.3)
    line, = plt.plot(xdata, xdata, 'b--')

    # create current point
    cur_pnt, = plt.plot(1, 1, 'gs')

    # setup  weights and alpha
    w1 = w2 = wb = random.random()
    alpha = 0.1

    while True:

        # choose a random point from sets A and B
        idx = random.randrange(len(DATA[0]))
        x = DATA[0][idx]
        y = DATA[1][idx]
        res = DATA[2][idx]

        # update weights
        obt = w1*x + w2*y - wb
        w1 += alpha*(res-obt)*x
        w2 += alpha*(res-obt)*y
        wb -= alpha*(res-obt)
        alpha *= 0.99

        # update line
        ydata = (wb - w1*xdata) / w2
        line.set_ydata(ydata)

        # update current point
        cur_pnt.set_data(x, y)

        # refresh plot
        plt.draw()
        plt.pause(0.001)


if __name__ == '__main__':
    main()
