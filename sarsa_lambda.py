import numpy as np

#########
# World #
#########

# STATES
# 1 2 3
# 4 5 6
# 7 8 9

S = 9 # number of states
A = 4 # number of actions

def reward(s):
    if s == 9:
        return 1.0
    elif s >= 1 and s <= 9:
        return 0.0
    else:
        return -1

def execute(s, a):
    if a == 1:      # go right
        ret = s+1
    elif a == 2:    # go down
        ret = s+3
    elif a == 3:    # go left
        ret = s-1
    elif a == 4:    # go up
        ret = s-3
    return ret if reward(ret) != -1 else s

def next_action(s):
    a = np.random.randint(1,5)
    return a


################
# Sarsa Lambda #
################

Q = np.zeros((S+1, A+1))
alfa = 0.1
gama = 0.9


for _ in xrange(1000):
    st = 1
    at = 1

    while reward(st) < 1:
        st_1 = execute(st, at)
        at_1 = next_action(st_1)
        rt_1 = reward(st_1)

        Q[st, at] += alfa*(rt_1+gama*Q[st_1, at_1]-Q[st,at])

        st = st_1
        at = at_1


###########
# Results #
###########

def print_board():
    for s in xrange(1, 10):
        for a in xrange(1, 5):
            print s, a, Q[s,a]
        print ""

print_board()