import numpy as np
from random import randrange, random
from math import exp
import matplotlib.pyplot as plt


def reinforcement_learning():
    V = np.zeros(7)
    T = 0.5
    ALPHA = 0.05

    while True:
        actual = randrange(5) + 1
        while actual >= 1 and actual <= 5:
            a = exp(V[actual-1]/T)
            b = exp(V[actual+1]/T)
            p_izq = a / (a+b)
            p_der = b / (a+b)

            desc = random()
            n_actual = actual-1 if desc <= p_izq else actual+1
            r = 1 if n_actual > 5 else 0

            V[actual] += ALPHA * (r + V[n_actual] - V[actual])
            actual = n_actual


def reinforcement_learning_plot():
    V = np.zeros(7)
    T = 0.5
    ALPHA = 0.05

    pnts, = plt.plot(range(7), V, '-b')
    plt.axis([1, 5, 0, 1.5])

    while True:
        pnts.set_data(range(7), V)
        plt.draw()
        plt.pause(0.01)
        actual = randrange(5)
        while actual >= 1 and actual <= 5:
            a = exp(V[actual-1]/T)
            b = exp(V[actual+1]/T)
            p_izq = a / (a+b)
            p_der = b / (a+b)

            desc = random()
            n_actual = actual-1 if desc <= p_izq else actual+1
            r = 1 if n_actual > 5 else 0

            V[actual] += ALPHA * (r + V[n_actual] - V[actual])
            actual = n_actual


def main():
    reinforcement_learning_plot()

main()