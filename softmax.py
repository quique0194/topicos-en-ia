import matplotlib.pyplot as plt
import numpy as np

N = 10
MIN_X = -10
MAX_X = 10

X = np.random.uniform(MIN_X, MAX_X, N)
X = np.sort(X)

P = np.zeros(N)

pnts, = plt.plot(X, P, 'b-')
plt.axis([MIN_X, MAX_X, -1, 1])

for T in np.arange(MIN_X, MAX_X, 0.1):
    for i in xrange(N):
        P[i] = np.exp(X[i]/T) / np.exp(X/T).sum()
        # P[i] = X[i] / X.sum()

    pnts.set_ydata(P)
    plt.draw()
    plt.pause(0.01)

    print P.sum()
