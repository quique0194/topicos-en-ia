from random import random, shuffle, randint
from numpy import zeros, arange

INF = 999999

def kmeans(k, data):
    # init centers
    centers = zeros(k)
    for i, center in enumerate(centers):
        centers[i] = data[randint(0, data.size-1)]
    
    # init categories
    categories = zeros(data.size)

    for _ in xrange(1000):
        # set categories
        for i, obj in enumerate(data):
            min = INF
            for j, center in enumerate(centers):
                dist = abs(obj-center)
                if dist < min:
                    min = dist
                    categories[i] = j

        # recalculate centers
        for j, center in enumerate(centers):
            suma = 0.0
            cont = 0
            for i, obj in enumerate(data):
                if categories[i] == j:
                    suma += obj
                    cont += 1
            centers[j] = suma/cont

    return centers



def main():    
    data = arange(100)
    shuffle(data)
    centers = kmeans(2, data)
    for center in centers:
        print center
    

main()